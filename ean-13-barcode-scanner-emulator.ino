#ifndef Bounce2_h
  #include <Bounce2.h>
#endif

#define KEY1 A2
#define KEY2 A3

const uint8_t BUTTON_PINS[2] = {KEY1, KEY2};
Bounce * buttons = new Bounce[2];
//=============================================================================
void setup() {
  Serial.begin(9600);
  buttons[0].attach(BUTTON_PINS[0], INPUT_PULLUP);
  buttons[0].interval(25); // задержка антидребезга
  buttons[1].attach(BUTTON_PINS[1], INPUT_PULLUP);
  buttons[1].interval(25); // задержка антидребезга
}
//=============================================================================
void loop() {
  buttons[0].update();
  if (buttons[0].fell()) {
    Serial.println("2200000000019"); // barcode fixed PLU=1
  }

  buttons[1].update();
  if (buttons[1].fell()) {
    Serial.println("2100002012341"); // barcode weighted
  }


}
//=============================================================================
